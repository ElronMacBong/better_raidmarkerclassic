## Interface: 11302
## LoadOnDemand: 0
## DefaultState: enabled
## Secure: 0

## Title: |cff278009Better|r RaidMarkerClassic
## Notes: Lets you mark and demark your target much easier.
## Author: Elron MacBong
## Version: 0.1.0

## SavedVariables: Better_RaidMarkerClassicDB
## SavedVariablesPerCharacter: Better_RaidMarkerClassicCharacterDB
## OptionalDeps: Blizzard_DebugTools, Blizzard_PetJournal

init.lua